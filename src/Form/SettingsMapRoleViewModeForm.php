<?php

namespace Drupal\view_mode_by_owner_role\Form;

use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure View mode by owner role settings for this site.
 */
class SettingsMapRoleViewModeForm extends BasicSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_mode_by_owner_role_settings_view_mode_map_mode';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['view_mode_by_owner_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $role_list = $this->choosenRoles();
    if (!isset($role_list)) {
      $link = Link::createFromRoute(
        $this->t('Settings choose role'),
        'view_mode_by_owner_role.settings_choose_role'
          );
      $form['description'] = [
        '#type' => 'item',
        '#markup' => $this->t(
          'You must select first the roles to use in the form @form_link.',
          ['@form_link' => $link->toString()],
        ),
      ];

      return $form;
    }

    $view_mode_map = $this
      ->config('view_mode_by_owner_role.settings')
      ->get('map_role_view_mode');

    if (!isset($view_mode_map) || count($view_mode_map) == 0) {

      $link = Link::createFromRoute(
        $this->t('Settings choose role'),
        'view_mode_by_owner_role.view_mode_settings'
      );
      $form['description'] = [
        '#type' => 'item',
        '#markup' => $this->t(
          'You must select first the view mode in the form @form_link.',
          ['@form_link' => $link->toString()],
        ),
      ];

      return $form;
    }
    $form['#tree'] = TRUE;
    $bundles = array_keys($view_mode_map);

    $form['main_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Settings'),
    ];

    foreach ($bundles as $bundle) {

      $form['view_mode_map'][$bundle] = [
        '#type' => 'details',
        '#title' => $bundle,
        '#group' => 'main_tabs',
      ];

      foreach ($view_mode_map[$bundle] as $view_mode_key => $view_mode_value) {

        $form['view_mode_map'][$bundle][$view_mode_key] = [
          '#type' => 'details',
          '#title' => $this->t('Configure @bundle @view_mode_value view mode',
          [
            '@bundle' => $bundle,
            '@view_mode_value' => $view_mode_key,
          ]),
        ];

        $view_mode_by_bundle = $this->listOfViewMode($bundle);
        foreach ($role_list as $role_key => $role) {

          $form['view_mode_map'][$bundle][$view_mode_key][$role_key] = [
            '#type' => 'select',
            '#title' => $this->t(
              'Choose the view mode that will be used when the owner has the role @role',
              ['@role' => $role]
            ),
            '#options' => $view_mode_by_bundle,
            '#default_value' => $view_mode_value[$role_key],
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config_to_save = $this->config('view_mode_by_owner_role.settings');

    $config_to_save->set(
      'map_role_view_mode',
      $form_state->getValue('view_mode_map')
    )->save();

    parent::submitForm($form, $form_state);
  }

}
